/**
 * Created  18/09/2018.
 *
 * @author simony2222
 */
public class Student {
	private final int    id;
	private       String firstName;
	private       String lastName;

	public Student(int id, String firstName, String lastName) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public final String getFirstName() {
		return firstName;
	}

	public final void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public final String getLastName() {
		return lastName;
	}

	public final void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public final int getId() {
		return id;
	}

	@Override
	public String toString() {
		return firstName + " " + lastName + " (" + id + ")";
	}

	public int compareTo(Student anotherStudent) {
		if (!this.getLastName().equals(anotherStudent.getLastName())) {
			return this.getLastName().compareTo(anotherStudent.getLastName());
		} else if (!this.getFirstName().equals(anotherStudent.getFirstName())) {
			return this.getFirstName().compareTo(anotherStudent.getFirstName());
		} else {
			return this.getId() - anotherStudent.getId();
		}
	}
}
