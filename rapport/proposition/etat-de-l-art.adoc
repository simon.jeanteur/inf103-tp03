=== Description de l’état de l’art
ifdef::env-gitlab,env-browser[:outfilesuffix: .adoc]


Notre projet est assez proche, par son fonctionnement, du Thérémine, créé dans
 les années 1920 et permettant à son utilisateur de produire de la musique en fonction
  des mouvements de sa main <<1>>. Contrairement au Thérémine, qui détecte le mouvement
   du musicien grâce aux ondes radio émises par l'instrument puis réfléchies par la main, nous avons décidé d’utiliser un capteur leap motion.
    Ce capteur reproduit un environnement  3D grâce  à trois caméras infrarouges.
    Il est fréquemment utilisé, notamment dans les domaines de la création graphique ainsi
    que pour l’interaction manuelle homme-machine <<2>>.

L’intégration au projet d’un synthétiseur sonore ainsi que d'une sortie MIDI permettra de
faire l’enregistrement des morceaux joués, et éventuellement d'y porter quelques modifications via un logiciel extérieur.

D’autres capteurs devront être positionnés sur le boîtier pour pouvoir interagir avec
l’objet. Cela se fait très bien avec une carte arduino, une carte électronique programmable
suffisamment puissante pour l’utilisation souhaitée <<3>>.
