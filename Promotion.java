import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created  18/09/2018.
 *
 * @author simony2222
 */
public class Promotion {
	private ArrayList<Student> studentArrayList;

	public Promotion(Student... students) {
		studentArrayList = new ArrayList(students.length);
		for (Student student : students) {
			studentArrayList.add(student);
		}
	}

	public int add(String firstName, String lastName) {
		int id = newId();
		studentArrayList.add(new Student(id, firstName, lastName));
		return id;
	}

	public int newId() {
		if (studentArrayList.isEmpty()) {
			return 0;
		} else {
			int maxid = studentArrayList.get(0).getId();
			for (Student student : studentArrayList) {
				if (student.getId() > maxid) {
					maxid = student.getId();
				}
			}
			return maxid + 1;
		}
	}

	public void printToConsole() {
		String out = "";
		for (Student student :
				studentArrayList) {
			out += student + "\n";
		}
		System.out.println(out);
	}

	private void swap(int i, int j) {
		if (i != j) {
			Student istudent = studentArrayList.get(i);
			studentArrayList.set(i, studentArrayList.get(j));
			studentArrayList.set(j, istudent);
		}
	}

	public void selectionSort() {
		int     indicePetit;
		Student min, sj;
		for (int i = 0; i < studentArrayList.size() - 1; i++) {
			indicePetit = i;
			min = studentArrayList.get(i);
			for (int j = i; j < studentArrayList.size(); j++) {
				sj = studentArrayList.get(j);
				if (sj.compareTo(min) < 0) {
					indicePetit = j;
					min = sj;
				}
			}
			swap(i, indicePetit);
		}
	}
}
