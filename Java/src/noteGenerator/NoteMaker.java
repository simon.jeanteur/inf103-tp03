package noteGenerator;

import arduinoCaptor.CaptorEvent;
import arduinoCaptor.CaptorEventListener;
import leapMotion.LeapMotion;
import util.Context;

import java.util.HashSet;
import java.util.Set;

public class NoteMaker implements CaptorEventListener {
	private final Set<NoteEventListener> noteEventListenerSet = new HashSet<>();
	private LeapMotion leapMotion;
	private Context context;

	@Override
	public void onCaptorEventStarts(CaptorEvent captorEvent) {

	}

	@Override
	public void onCaptorEventEnds(CaptorEvent captorEvent) {

	}
}