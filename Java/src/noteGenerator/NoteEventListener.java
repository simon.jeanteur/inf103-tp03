package noteGenerator;


public interface NoteEventListener {
	void onNoteEventStart(NoteEvent noteEvent);
	void onNoteEventEnds(NoteEvent noteEvent);
}
