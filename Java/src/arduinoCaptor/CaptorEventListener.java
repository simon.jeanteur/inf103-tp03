package arduinoCaptor;

public interface CaptorEventListener {
	void onCaptorEventStarts(CaptorEvent captorEvent);
	void onCaptorEventEnds(CaptorEvent captorEvent);
}
