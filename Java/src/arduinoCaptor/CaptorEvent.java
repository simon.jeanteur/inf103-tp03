package arduinoCaptor;

public class CaptorEvent {
	private int octave;
	private double shade;

	public int getOctave() {
		return octave;
	}

	public double getShade() {
		return shade;
	}
}
