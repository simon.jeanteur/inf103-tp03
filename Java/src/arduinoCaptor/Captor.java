package arduinoCaptor;

import java.util.HashSet;
import java.util.Set;

public class Captor {
	private final Set<CaptorEventListener> captorEventListenerList = new HashSet<>();

	public void addOnCaptorEventListener(CaptorEventListener captorEventListener){
		captorEventListenerList.add(captorEventListener);
	}
}
