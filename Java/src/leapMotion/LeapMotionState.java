package leapMotion;

public class LeapMotionState {
	public enum Note {
		DO, RE, MI, FA, SOL, LA, SI,
		DO_SHARP, RE_SHARP, MI_SHARP, FA_SHARP, SOL_SHARP, LA_SHARP, SI_SHARP,
		DO_FLAT, RE_FLAT, MI_FLAT, FA_FLAT, SOL_FLAT, LA_FLAT, SI_FLAT,
	}

	private Note note;

	public Note getNote() {
		return note;
	}
}
